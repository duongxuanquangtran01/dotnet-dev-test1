﻿using System.Text;

Console.WriteLine("Enter two strings: ");
Console.WriteLine("Enter first string: ");
string value1 = Console.ReadLine();
Console.WriteLine("Enter second string: ");
string value2 = Console.ReadLine();
var result = PracticeStrings.MixTwoStrings(value1, value2);
Console.WriteLine("Mixed string: " + result);

public class PracticeStrings
{

    public static string MixTwoStrings(string value1, string value2)
    {
        StringBuilder sb = new StringBuilder();
        string longerString;
        if (value1.Length >= value2.Length)
        {
            longerString = value1;
        }
        else
        {
            longerString = value2;
        }
        for (int i = 0; i < longerString.Length; i++)
        {
            if (i < value1.Length)
            {
                sb.Append(value1[i]);
            }

            if (i < value2.Length)
            {
                sb.Append(value2[i]);
            }

        }
        return sb.ToString();
    }
}


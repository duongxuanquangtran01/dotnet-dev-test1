﻿namespace SortArrayDESC;

public class Program
{
    static void Main(string[] args)
    {
        var sorted = SortArrayDesc(new int[] { 1, 9, 6, 7, 5, 9 });
        string result = string.Join(" ", sorted);
        Console.WriteLine(result);
    }

    public static int[] SortArrayDesc(int[] x)
    {
        Array.Sort<int>(x, new Comparison<int>(
                  (i1, i2) => i2.CompareTo(i1)));

        return x;
    }


}
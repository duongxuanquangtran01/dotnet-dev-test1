﻿using System.Runtime.ExceptionServices;

namespace IndexOfSecondLargestElementInArray;

public class Program
{
    static void Main(string[] args)
    {
        var myArr = new[] { 0, 1, 2, 3, 13, 8, 13 };
        var result = IndexOfSecondLargestElementInArray(myArr);
        Console.WriteLine(result);
    }

    public static int IndexOfSecondLargestElementInArray(int[] x)
    {
        if (x.Length == 0 || x.Length == 1)
        {
            return -1;
        }
        else
        {
            int a1;
            int count = 0;
            int[] myArray = x;
            int largest = int.MinValue;
            int second = int.MinValue;
            foreach (int i in myArray)
            {
                if (i > largest)
                {
                    second = largest;
                    largest = i;
                }
                else
                {
                    if (i > second)
                        second = i;
                    if (largest == second)
                    {

                        for (int a = 0; a < myArray.Length && count < 2; a++)
                        {
                            if (myArray[a] == second)
                            {
                                a1 = a;
                                count++;
                                if (count == 2)
                                {
                                    return a1;
                                }
                            }
                        }
                    }
                }
            }

            System.Console.WriteLine(second);
            int index = Array.IndexOf(myArray, second);
            return index;
        }
    }
}